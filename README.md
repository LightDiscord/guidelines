# ![logo](https://cdn.discordapp.com/attachments/359076126288773120/360454005517844480/server-icon.png) Islands Wars


> Islands Wars is a minecraft multiplayer server network.
> This project contains the basic development rules and how to contribute!


# Getting started

These instructions will get you a copy of the project up. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them
* [GIT] is a version control system.
* [JDK] is the Java Development Kit, Islands Wars need version 8 or higher (maybe 9 soon ;).

### Git Clone

git clone [url] [options]
```shell
$ git clone git@gitlab.com:islands-wars/GROUP/REPOSITORY_NAME.git DirFoldername
```
You can now import this project with your favorite Java IDE. Make sure to import the project as Gradle Project and run ``gradle build`` to import all dependencies if needed!

### Deployment

Compile your modifications using gradle :
```shell
./gradlew <task> (on Unix-like platforms such as Linux and Mac OS X)
gradlew <task> (on Windows using the gradlew.bat batch file)
```
We use git flow as branching-model, simply init a new feature :
```shell
git flow feature start featureName
git add .
git commit 
..
git flow feature publish featureName
```
And then open a pull request linking to your feature brench


# Contributing 

How to contribute and project guidelines.

### Coding guidelines

Adopting the [Google Java Style] with the following changes:

```
3
    A source file consists of, in order:

      * Package statement
      * Import statements
      * Default generated header (Cf 2.2 Header)
      * Exactly one top-level class
      
      Exactly one blank line separates each section that is present.

4.2
    Our block indent is +4 characters

4.4
    Our line length is 200 characters.
```
Also, in order to be read by everybody, you have to work in English. Be concise when naming method, var, class according to Java standard naming conventions.
And do not commit with crazy message like WIP, explain what you did :p
Please use our project's [code style] and import him in Intellij Idea

### Header

Import and use this [header] when contributing, or your code will be rejected.

### Protected branch

Whatever the repository is, ```master``` branch is always protected, only an admin can accept merge request.
Please work on a dedicatedgit flow branch.

# Javadoc

Comment every public method you create (in english :/) and also whatever you judge necessary (a long stream for example)

# Project dependencies 

This section is dedicated to internal contributor. **If you are an external contribuor, you may skip this part and go [here](https://gitlab.com/islands-wars/Guidelines/edit/master/README.md#gradle-configuration) to configure your project (not needed if you clone existing project).**  
We're using custom dependencies that have only a private access (not at all). 
When you join the development team, you will have an username and a password access to our private repository. In this example, we will use ```username: vinetos``` and ```password: myPass```. 

### IslandsWars properties file

First, create a file into your ``userhome/.m2`` called ``islandswars.properties``. **Change with your credentials.**
```properties
IS_PUBLIC_URL=http://repo.islandswars.fr/artifactory
IS_PUBLIC_USERNAME=vinetos
IS_PUBLIC_PASSWORD=myPass
```

### Gradle configuration

Then, edit your **buid.gradle** to add our repository settings : 
```gradle
// ...
apply from: 'islandswars.gradle'

buildscript {
    repositories {
        jcenter()
    }
    dependencies {
        classpath(group: 'org.jfrog.buildinfo', name: 'build-info-extractor-gradle', version: '4.+')
    }
}

// ...
```

Create a new file at the root of your project called **islandswars.gradle** :
```gradle
// Custom artifactory file
// Don't edit manually ! Will broke the build
ext.guest = true
ext.isProps = new Properties()

allprojects {
    apply plugin: "com.jfrog.artifactory"
}

artifactory {
    // IslandsWars repository
    loadCredentials()
    contextUrl = guest ? 'http://repo.islandswars.fr/artifactory' : isProps['IS_PUBLIC_URL']   //The base Artifactory URL if not overridden by the publisher/resolver
    resolve {
        repository {
            repoKey = 'islandswars'
            if(!guest) {
                username = isProps['IS_PUBLIC_USERNAME']
                password = isProps['IS_PUBLIC_PASSWORD']
            }
            maven = true
        }
    }
}

def loadCredentials() {
    String userHome = System.getProperty("user.home")
    File isSettings = new File(userHome, ".m2/islandswars.properties")
    if(isSettings.exists()) {
        guest = false
        isProps.load(isSettings.newReader())
    }
}

```

**Import changes** and you can now add our dependencies like : 
```gradle
dependencies {
    compile 'fr.islandswars:projectname:projectversion'
}
```
See also this default [build] file and the [ci] file you must add to yours project.

# License

Copyright (c) 2017 - 2018 Islands Wars.

# Authors

* **[Vinetos](https://twitter.com/vinetos)**

  * Initial works, lead dev
* **[Xharos](https://twitter.com/devxharos)**

  * Initial works
* **[LightDiscord](https://twitter.com/LightDiscord)**

  * Initial works on web site

   [Google Java Style]: <https://google.github.io/styleguide/javaguide.html>
   [code style]: <https://gitlab.islandswars.fr/dev/Guidelines/blob/master/is_scheme.xml>
   [header]: <https://gitlab.islandswars.fr/dev/Guidelines/blob/master/HEADER>
   [build]: <https://gitlab.islandswars.fr/dev/Guidelines/blob/master/build.gradle>
   [ci]: <https://gitlab.islandswars.fr/dev/Guidelines/blob/master/gitlab-ci.yml>
   [git]: <https://git-scm.com/book/en/v1/Getting-Started-Installing-Git>
   [jdk]: <http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html>